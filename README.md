# **Introduction**
------
To backup the data to another disk directory.

# **Usage**
------
## Automatic method
Run the installation script
```
$ ./install
```

## Edit method
Firstly, make sure the rsync tool is available and overview the backup disk.
```
$ rsync --version 
$ df -h
```
To see the cron job.
```
$ crontab -l
```
Add lines to make some backup job.
```
$ crontab -e
```
Choose vim as the text editor and add some lines as following.
```
$ 0 22 * * * rsync -av --delete /to/the/source /to/the/destination/
```
Or you can backup every two hour as following.
```
$ 0 */2 * * * rsync -av --delete /to/the/source /to/the/destination/
```
Or backup all your home and next time just mount this as home.
```
$ 0 */2 * * * rsync -av --delete /home/ /backup/
```
Make sure you backup your workspace, hive, key and password store.


# **Author**

Jason Chen
